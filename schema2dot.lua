--[[
--  Copyright 2013-2017 Pavel Kraynyukhov, new WEB() LLP
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
--  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.
--
--
-- generate dot output for schema.json
--]]


-- импортируем те-же самые модули
local rest=require("rest")
local JSON=require("JSON")
local utils=require("utils")

-- Объявляем функцию чтения из файла всех данных сразу в один строковый объект
function readAll(file)
  local f = io.open(file, "rb")
  local content = f:read("*all")
  f:close()
  return content
end

-- Загружаем, ранее подготовленную схему 
-- и десериализируем строку из JSON в нативное представление
local schema=JSON:decode(readAll("schema.json"));

-- Вспомогательная функция определения обязательности поля
function mandatory2sign(boolstr)
  if(utils.isempty(boolstr))
  then
    print('Usage: mandatory2sign(boolstr), where boolstr is a "true" or "false" - string value');
    os.exit(1)
  end
  if boolstr == "true"
  then
    return "*"
  end
  return ""
end

-- Вспомогательная функция определения объекта "только для чтения"
function read_only2sign(boolstr)
  if(utils.isempty(boolstr))
  then
    print('Usage: read_only2sign(boolstr), where boolstr is a "true" or "false" - string value');
    os.exit(1)
  end
  if boolstr == "true"
  then
    return "-"
  end
  return "+"
end

local references={}
local inheritance={}

-- Формирование заголовка dot-файла, с аттрибутами графа
print([[digraph G {
  graph [rankdir=BT, margin="0.0,0.0", ranksep=0.5,nodesep=0.5, fontsize=12, ratio="fill", orientation=portrait ]
  subgraph cluster_model {
    graph [style=dotted];
    node [shape=record,fillcolor="#b7c9e3",style=filled];
    edge [arrowhead=onormal,weight=1.4];
]])

-- Формирование усзлов-таблиц,с соответствующими атрибутами
for table_name,attributes in pairs(schema) do
  local label='[label="{'..table_name..'|{{';
  for attr_name,attr_value in pairs(attributes)
  do
    if(attr_name ~= "super")
    then
      label=label..'<'..attr_name..'> '..read_only2sign(attr_value.read_only)..attr_name..':'..attr_value.type..'['..attr_value.max_length..']('..attr_value.label..')'..mandatory2sign(attr_value.mandatory)..'|';
      if(not utils.isempty(attr_value.reference))
      then
        references[table_name..':'..attr_name]=attr_value.reference.value
      end
    else
      table.insert(inheritance,{["child"]=table_name,["parent"]=attr_value})
    end
  end
  label=label:sub(1,label:len()-1);
  label=label..'}}}"]'
  print(table_name.." "..label)
end

-- Формирование графа наследования
for i=1,#inheritance
do
  print(inheritance[i].child.." -> "..inheritance[i].parent)
end

-- Формирование графа реляционных связей
for tab_attr,tab_name in pairs(references)
do
  local color=string.format("%x",math.floor((16777215-255)*math.random()+254))
  print('edge [dir=both, arrowhead=none, weight=4, arrowtail=crow,color="#'..color..'"] '..tab_attr..' -> '..tab_name..';')
end

-- закрытие dot-файла.
print([[
}}
]])
