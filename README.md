# README #

Lua scripts for downloading and vizualizing Service Now Data Model. Th vizualization itself is done with graphviz. 
The lua code is used to download the schema and generate a dot file (will be redirected to stdout).


### Requirements ###

* Linux or cygwin with following applications:
* 	lua or luajit
* 		Lua modules: 
* 			luasec
* 			native lua JSON from Jeffrey Friedl (http://regex.info/blog/lua/json)
* 	graphviz

### Setup ###

Assuming you have all prerequisites installed and ready to use.

* Checkout this repository: git clone https://pk_nw@bitbucket.org/pk_nw/servicenow_schemaviz.git
* Change directory to servicenow_schemaviz
* Edit snow_entities.lua and add getSchema calls for the table of interest (this will recursively map all relationships and inheritance into entities lua table)
* Example: add getSchema(entities,'incident') before the "writeAll("schema.json",JSON:encode_pretty(entities))" sentence at the end of the file
* 
* run the shell command to download the schema: luajit snow_entities.lua username password your_snow_instance
* Grab a coffee, walk around or do something uslefull while schema is referencing and downloading
* 
* run the shell command to generate the SVG file:  luajit schema2dot.lua | dot -Tsvg > schema.svg
* or: luajit schema2dot.lua | dot -Tsvg -Gsize=46.8,33.1\! > schema.svg
* later will generate an A0 size landscape oriented SVG file
* 
* Now just open he file in google-chrome and zoom in/out press Ctrl-F to find tables and columns

