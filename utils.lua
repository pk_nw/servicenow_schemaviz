--- Common utilities found in internet
-- @module utils
-- @author  lua community
-- @copyright public domain
-- @release $Id: deb58d6750eefc7330018c2b23c87c9a8bc5c98d $
-- vim: ts=2 tabstop=2 shiftwidth=2 expandtab
-- vim: retab 

local utils={}

function utils.isempty(s)
  return (type(s) == "table" and next(s) == nil) or s == nil or s == ''
end

function utils.split(s,pattern)
  local result={}
  for i in string.gmatch(s,pattern)
  do
    table.insert(result,i)
  end
  return result;
end

function utils.int2bytes(num, width)
  local function _n2b(width, num, rem)
    rem = rem * 256
    if width == 0 then return rem end
    return rem, _n2b(width-1, math.modf(num/256))
  end
  return string.char(_n2b(width-1, math.modf(num/256)))
end

function utils.istabempty(t)
  if type(t) == table and next(t) == nil
  then
    return true
  end
  return false
end

function utils.tabHasKey(tab,key)
  local result=false;
  if not utils.istabempty(tab)
  then
    for key_,_ in pairs(tab)
    do
      if(key_ == key)
      then
        result=true;
        return true;
      end
    end
  end
  return result;
end

function utils.xml_escape(str)
  return ((((str:gsub("&","&amp;")):gsub([["]],"&quot;")):gsub("'","&apos;")):gsub("<","&lt;")):gsub(">","&gt;");
end

function utils.xml_unescape(str)
  return ((((str:gsub("&amp;","&")):gsub("&quot;",[["]])):gsub("&apos;","'")):gsub("&lt;","<")):gsub("&gt;",">");
end

-- LE int to bytes  written by Tom N Harris. 
-- http://lua-users.org/wiki/ReadWriteFormat
function utils.int2bytes(num, width)
  local function _n2b(width, num, rem)
    rem = rem * 256
    if width == 0 then return rem end
    return rem, _n2b(width-1, math.modf(num/256))
  end
  return string.char(_n2b(width-1, math.modf(num/256)))
end


return utils;
