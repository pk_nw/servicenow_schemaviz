--[[
--  Copyright 2013-2017 Pavel Kraynyukhov <pk@new-web.co>, new WEB() LLP
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
--  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.
--
--
-- ServiceNow REST API wrapper
--]]
local socket = require("socket")
local ssl = require("ssl")
local https = require("ssl.https")

local ltn12=require("ltn12")
local utils=require("utils")
local b64=require("base64")
local JSON=require("JSON")

local rest={}

function rest.post(user,password,URL,json)
  local response={}
  print(URL)
  print(json)
  print(string.len(json))

  local res, code, headers, status=https.request{
    url = URL,
    method = "POST",
    headers = {
      ["Content-Type"] = "application/json",
      ["Accept"] = "application/json",
      ["Authorization"] = "Basic "..b64.encode(user..":"..password),
      ["Content-Length"] = string.len(json),
    },
    source = ltn12.source.string(json),
    sink = ltn12.sink.table(response)
  }
  if code == 200 or code == 201 
  then
    return JSON:decode(table.concat(response," "))
  else
    print("===============================================")
    print(JSON:encode_pretty({"Error",res,code,headers,status,response}))
    print("===============================================")
    return nil
  end
end

function rest.put(user,password,URL,json)
  local response={}
  print(URL)
  print(json)
  print(string.len(json))

  local res, code, headers, status=https.request{
    url = URL,
    method = "PUT",
    headers = {
      ["Content-Type"] = "application/json",
      ["Accept"] = "application/json",
      ["Authorization"] = "Basic "..b64.encode(user..":"..password),
      ["Content-Length"] = string.len(json),
    },
    source = ltn12.source.string(json),
    sink = ltn12.sink.table(response)
  }
  if code == 200 or code == 201 
  then
    return JSON:decode(table.concat(response," "))
  else
    print("===============================================")
    print(JSON:encode_pretty({"Error",res,code,headers,status,response}))
    print("===============================================")
    return nil
  end
end

function rest.get(user,password,URL)
  if utils.isempty(URL)
  then
      error("rest.get(): URL is the mandatory argument")
      return nil
  end

  local response_body={}
  local res, code, headers, status=https.request{
    url=URL,
    method="GET",
    headers = {
      ["Content-Type"] = "application/json;charset=UTF-8",
      ["Accept"] = "application/json",
      ["Authorization"] = "Basic "..b64.encode(user..":"..password)
    },
    sink = ltn12.sink.table(response_body)
  }
  return code, response_body
end


return rest
