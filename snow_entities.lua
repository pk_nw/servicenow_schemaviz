--[[
--  Copyright 2013-2017 Pavel Kraynyukhov, new WEB() LLP
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
--  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.
--
--
-- generate schema.json file
--]]

local rest=require("rest")
local JSON=require("JSON")
local utils=require("utils")

--[[
function readAll(file)
  local f = io.open(file, "rb")
  local content = f:read("*all")
  f:close()
  return content
end
]]--

function writeAll(file,content)
  local f = io.open(file, "w")
  local content = f:write(content)
  f:close()
end

local user=""
local password=""
local instance="steldemo1"

function httpGETTableProperties(table_name)
  if(utils.isempty(table_name))
  then
    print("Usage: httpGETTableProperties(table_name)");
    os.exit(1)
  end
  local response="";
  local code,resp=rest.get(user,password,"https://"..instance..".service-now.com/api/now/table/sys_dictionary?sysparm_query=elementISNOTEMPTY^name="..table_name);
  if(code == 200)
  then
    for i=1,#resp
    do
      response=response..resp[i]
    end
  end
  return code,response
end

function httpGETDBObjectByName(table_name)
  if(utils.isempty(table_name))
  then
    print("Usage: httpGETSuper(table_name)");
    os.exit(1)
  end

  local response="";
  local code,resp=rest.get(user,password,"https://"..instance..".service-now.com/api/now/table/sys_db_object?sysparm_query=name="..table_name);
  if(code == 200)
  then
    for i=1,#resp
    do
      response=response..resp[i]
    end
  end
  return code,response
end

function httpGETDBObjectByID(sys_id)
  if(utils.isempty(sys_id))
  then
    print("Usage: httpGETDBObjectByID(sys_id)");
    os.exit(1)
  end

  local response="";
  local code,resp=rest.get(user,password,"https://"..instance..".service-now.com/api/now/table/sys_db_object?sysparm_query=sys_id="..sys_id);
  if(code == 200)
  then
    for i=1,#resp
    do
      response=response..resp[i]
    end
  end
  return code,response
end

local entities={}

function getSchema(entities,table_name,depth) -- Загружаем дерево объектов схемы БД во внешний ассоциативный массив entities
  if(utils.isempty(table_name))
  then
    return nil;
  end
  -- Исключаем sys_domain из обработки
  if(table_name == 'sys_domain')
  then
    return nil;
  end
  if(utils.isempty(depth))
  then
    depth=6
  end
  if(depth == 0)
  then
    return nil;
  end

  depth=depth-1;

  if(utils.tabHasKey(entities,table_name)) -- не выгружаем информацию из инстанса, если запис о требуемой таблице уже есть в entities
  then
    return nil;
  end

  local code, data=httpGETTableProperties(table_name); -- Получаем свойства таблицы
  if(code == 200) -- Если нет HTTP ошибок, то обрабатываем результат
  then
    local jobj=JSON:decode(data) -- преобразуем полученные данные из JSON строки в нативные объекты
    for i=1,#jobj.result  -- В ответ на каждый запрос к API возвращаемыe Service Now JSON объекты, завёрнуты в массив result
    do
      if(utils.isempty(entities[jobj.result[i].name])) -- Если в массиве entities, ещё нет записи о таблице с именем jobj.result[i].name
      then
        entities[jobj.result[i].name]={} -- то инициализируем подобную запись пустым массивом
      end
      --[[
          Вносим в массив сущностей необходимую нам информацию о колонке (где jobj.result[i].element содержит имя колонки):
          label - англоязычный лейбл поля
          max_length  - максимально доступная длинна помещаемых данных
          mandatory - индикатор того является ли поле обязательным
          read_only - индикатор доступности поля только для чтения
          type - тип поля
          reference - ссылка на внешний объект БД (пусто если тип не является reference)
      --]]
      entities[jobj.result[i].name][jobj.result[i].element]={
        ["label"]=jobj.result[i].column_label,
        ["max_length"]=jobj.result[i].max_length,
        ["mandatory"]=jobj.result[i].mandatory,
        ["read_only"]=jobj.result[i].read_only,
        ["reference"]=jobj.result[i].reference,
        ["type"]=jobj.result[i].internal_type.value
      }
      -- Если поле reference не пусто (т.е. данная колонка является ссылкой на запись в другой таблице)
      -- рекурсивно получаем схему связей для данной таблицы
      if(not utils.isempty(entities[jobj.result[i].name][jobj.result[i].element].reference))
      then
        if(not utils.tabHasKey(entities,entities[jobj.result[i].name][jobj.result[i].element].reference.value))
        then
          getSchema(entities,entities[jobj.result[i].name][jobj.result[i].element].reference.value,depth)
        end
      end
    end
    -- Получаем sys_id обрабатываемой таблицы, и её суперкласс (если есть)
    local ncode,self=httpGETDBObjectByName(table_name)
    if(ncode == 200)
    then
      local jself=JSON:decode(self).result[1]
      if(not utils.isempty(jself))
      then
        if(not utils.isempty(jself.super_class)) -- если у таблицы есть суперкласс, то получаем его sys_id
        then
          local rcode,super=httpGETDBObjectByID(jself.super_class.value)
          if(rcode == 200)
          then
              local ntab=JSON:decode(super).result[1].name
              entities[table_name]["super"]=ntab  -- Добавляем информацию о суперклассе текущей таблицы, в аттрибут super
              if(not utils.tabHasKey(entities,ntab)) -- Если информации о суперклассе ещё нет в схеме, 
                                                     -- то рекурсивно выгружаем схему связей для суперкласса
              then
                getSchema(entities,ntab,depth)           -- Выгружаем только суперкласс, но не его суперклассы
              end
          end
        end
      end
    end
  end
end



if(#arg <2)
then
  print("Usage: luajit snow_entities.lua user password [instance]")
  print("       if instance is ommited, then "..instance.." will be used");
  os.exit(1)
else
  user=arg[1]
  password=arg[2]
  if(#arg >2)
  then
    instance=arg[3]
  end
end


--[[
-- call getSchema once or several times to get all the entities and their respective
-- relationships into entities object.
--
--]]

getSchema(entities,'u_astel_services',1)

writeAll("schema.json",JSON:encode_pretty(entities))
